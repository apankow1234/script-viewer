# Script-Viewer

## Convert and View Scripts for Live Broadcast, Film/TV, and Teleprompters

[Live Test](https://apankow1234.gitlab.io/script-viewer/)

![What it makes](documentation/renders-001.png)

![How it works](documentation/code-002.png)

![How it works](documentation/code-003.png)

![How it works](documentation/code-001.png)