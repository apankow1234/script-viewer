window.onload = () => {
    const scripts  = document.getElementsByClassName('script');
    const formats  = ['broadcast','prompter','film'];
    const controls = document.forms['view-controls'];
    let format_selections = document.getElementById('format-selection');

    formats.forEach( (f) => {
        let input_html = `
            <input type="button" name="type" id="toggle-${f}" value="${f}">
        `;
        format_selections.innerHTML += input_html;
    } );

    format_selections.querySelectorAll('input').forEach( (r) => {
        r.onclick = (evt) => {
            for( let s=0; s < scripts.length; s++ ) {
                for( let f=0; f < formats.length; f++ ) {
                    scripts[s].classList.remove(formats[f]);
                }
                scripts[s].classList.add(evt.target.value);
            }
        }
    } );
    format_selections.querySelectorAll('input')[0].checked = true;
    
    let screen_toggle = document.getElementById('toggle-fullscreen');
    screen_toggle.onclick = (evt) => {
        evt.preventDefault();

        const fmt = 'for-print';
        let for_screen = scripts[0].classList.contains(fmt) ? true : false;

        if( for_screen ) {
            [...scripts].forEach( (script) => {
                script.classList.remove( fmt );
            } );
            evt.target.innerText = "For Print";
        } else {
            [...scripts].forEach( (script) => {
                script.classList.add( fmt );
            } );
            evt.target.innerText = "For Screen";
        }
    }
}